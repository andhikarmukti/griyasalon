<?php

namespace Database\Factories;

use App\Models\Premium;
use Illuminate\Database\Eloquent\Factories\Factory;

class PremiumFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Premium::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
