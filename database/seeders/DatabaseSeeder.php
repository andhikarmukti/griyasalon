<?php

namespace Database\Seeders;

use App\Models\Basic;
use App\Models\Premium;
use App\Models\PaketWedding;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        for($i=1; $i < 51; $i++){
            PaketWedding::create([
                'jenis_paket' => 'basic',
                'demo_template' => 'demo ' . $i
            ]);
        }

        for($i=51; $i < 58; $i++){
            PaketWedding::create([
                'jenis_paket' => 'premium',
                'demo_template' => 'premium ' . $i
            ]);
        }

        for($i=51; $i < 58; $i++){
            PaketWedding::create([
                'jenis_paket' => 'VIP',
                'demo_template' => 'premium ' . $i
            ]);
        }
    }
}
